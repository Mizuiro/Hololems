﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA;
using HoloToolkit.Unity.InputModule;
using UnityEngine.XR.WSA.Input;

public class Razer : MonoBehaviour{
    LineRenderer line;
    public GameObject dest;
    public Vector3 hand;
    RaycastHit hit;
    float counter = 0;
    // Use this for initialization
    void Start () {
        line = GetComponent<LineRenderer>();
        Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
        int distance = 10;
        if (!Physics.Raycast(ray, out hit, distance))
        {
            Destroy(gameObject);
        }
        else
        {
            line.SetPosition(0, hand);
        }
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = hand + Vector3.Normalize( hit.point - hand ) * counter;
        line.SetPosition(1, transform.position);
        line.SetPosition(0, hand + Vector3.Normalize(hit.point - hand) * (counter - 1));
        counter += Time.deltaTime * 5;
        if( Vector3.Distance( transform.position,hit.point ) < 0.4f)
        {
            var n = Instantiate(dest, hit.point + hit.normal * 0.05f, Quaternion.FromToRotation(Vector3.back, hit.normal));
            n.transform.localEulerAngles = new Vector3(n.transform.localEulerAngles.x, n.transform.localEulerAngles.y, n.transform.localEulerAngles.z);
            Destroy(gameObject); 
        }
    }

 

}
