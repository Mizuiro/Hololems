﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA;
using HoloToolkit.Unity.InputModule;
using UnityEngine.XR.WSA.Input;

public class RayPoint : MonoBehaviour,IInputClickHandler, IHoldHandler
{
    public GameObject Razer;
    Vector3 hand;
    float Timer = -1;


	// Use this for initialization
	void Start () {
        InteractionManager.InteractionSourceUpdated += InteractionSourceUpdated;
    }
	void Update()
    {
        Timer -= Time.deltaTime;
    }
    public void OnInputClicked(InputClickedEventData eventData)
    {
            var n = Instantiate(Razer);
            n.GetComponent<Razer>().hand = hand;
    }
    private void InteractionSourceUpdated(InteractionSourceUpdatedEventArgs ev)
    {
        Vector3 position;
        if (ev.state.sourcePose.TryGetPosition(out position))
        {
            // オブジェクトの位置を手の位置に更新
            hand = position;

        }
    }
    public void OnHoldStarted(HoldEventData eventData)
    {
        //ホールドタップしたときに呼ばれるメソッド
    }

    public void OnHoldCompleted(HoldEventData eventData)
    {
 
    }

    public void OnHoldCanceled(HoldEventData eventData)
    {

    }

}
