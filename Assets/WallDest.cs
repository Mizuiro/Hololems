﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallDest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.localScale -= Vector3.one * Time.deltaTime / 8;
        if (transform.localScale.x < 0)
        {
            Destroy(gameObject);
        }
	}
}
