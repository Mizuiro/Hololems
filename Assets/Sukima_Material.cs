﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sukima_Material : MonoBehaviour {
    Material a;
	// Use this for initialization
	void Start () {
        a = GetComponent<MeshRenderer>().material;
	}
	
	// Update is called once per frame
	void Update () {
        a.mainTextureOffset += new Vector2( Time.deltaTime / 50, Time.deltaTime / 100);
	}
}
